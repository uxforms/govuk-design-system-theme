# govuk-design-system-theme

## Building

Built using `npm`. Use the version specified in `.tool-versions`, or (recommended) use asdf to ensure you're using the right version.

### Build

`npm run-script build`

### Versioning

Use `npm version`. 

E.g. `npm version patch` to bump to the next patch version number.

## Templates

We've kept as close to the original Nunjucks templates as possible. But inevitably there are places where we need to diverge.

### Macros 

Macros are approximated by having their template in a named subdirectory, and their parameters renamed from `param`
 to the name of the macro. E.g. where the original Nunjucks `header` macro uses `param.classes` our template uses `govukHeader.classes`.
 This is to avoid name clashes between template/macro parameters.
 
### Template logic

Some of the Nunjucks templates contain logic that can't sensibly be reproduced directly within Mustache, so we've 
introduced new parameters which need to implement the logic instead. We've flagged each of these with a comment in the form:
`{{! WARNING: This value must be explicitly added to the template context - }}`  


### Internationalisation

Some of the original Nunjucks templates contain hard-coded English text. We've had to introduce template parameters to replace them.

### Collections

If a template expects a collection, you should also add a `collectionName.nonEmpty -> true` value to the map, too, 
so the Mustache template has something to test to see if there are items in the collection. 

### UX Forms-specific templates

There are some things that UX Forms needs to render that are not included in GOV.UK's Design System, e.g. templates to 
render Sections and Pages. These have been put in their own `uxforms` directory to make it clear which bits are direct
translations from GOV.UK and which bits have been added to make the rest of UX Forms work. 

## Hard-coding

Asset paths within the css are set at sass compile time. Due to the assets being served from one domain and the form from another,
unfortunately this is always going to cause problems.

The asset path sass variable is hard-coded to the **Production** CDN, based on this theme's name. This should be OK in the 
overwhelming majority of cases but will look odd when, say, you're upgrading to a newer version of GOV.UK's design system and they've 
changed the images.

## Changes to push back to GOV.UK's templates:

#### `textarea/template.mustache`

There's no way to add attributes to the top-level formgroup div. Which we need to set an `id` on to drive runtime 
widget visibility.
https://github.com/alphagov/govuk-design-system/issues
  
#### `input/template.mustache`

Same as for textarea.
