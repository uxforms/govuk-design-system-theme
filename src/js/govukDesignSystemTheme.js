import {initUXForms} from "./uxforms";
import {initAll} from "govuk-frontend";

globalThis.GOVUKDesignSystemTheme = {
  init: function (containerUrl) {
    initAll();
    initUXForms(containerUrl);
  }
}