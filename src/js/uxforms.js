function refreshVisibility(containerUrl) {

  const getVisibilities = async () => {
    const response = await fetch(containerUrl, {
      method: 'POST',
      body: new URLSearchParams(new FormData(document.querySelector('form')))
    })

    if (response.ok) {
      const json = await response.json();
      return Promise.resolve(json)
    } else {
      return Promise.reject("Unable to parse json response from UX Forms")
    }
  }

  getVisibilities().then(
    (json) => {
      for (key in json) {
        const e = document.querySelector(`#${key}`);
        if (e !== null) {
          if (json[key].hidden) {
            e.classList.add("hidden");
          } else {
            e.classList.remove("hidden");
          }
        }
      }
    }
  )
}

function attachVisibilityListeners(containerUrl) {
  document.querySelectorAll('input,textarea,select').forEach(e =>
    e.addEventListener('change', () =>
      refreshVisibility(containerUrl)
    )
  )
}

function attachStatePersistenceListeners() {
  document.querySelectorAll('[data-module="govuk-tabs"] .govuk-tabs__list-item').forEach(t =>
    t.addEventListener('click', e => {
      const index = [...e.target.closest('.govuk-tabs__list').querySelectorAll('.govuk-tabs__list-item')]
        .findIndex(item => item === e.target.closest('.govuk-tabs__list-item'))
      e.target.closest('[data-module="govuk-tabs"]').querySelector('input[type="hidden"]').value = index;
    })
  );
}

function initializeTabToPersistedValue() {
  document.querySelectorAll('[data-module="govuk-tabs"]').forEach((e) => {
      const persistedHiddenField = e.querySelector('input[type="hidden"]');
      if (persistedHiddenField !== null) {
        const index = persistedHiddenField.value;
        if (index !== null) {
          const item = e.querySelectorAll('.govuk-tabs__tab').item(index);
          if (item !== null) {
            item.click();
          }
        }
      }
    }
  )
}

export function initUXForms(containerUrl) {
  attachVisibilityListeners(containerUrl);
  attachStatePersistenceListeners();
  initializeTabToPersistedValue();
}
