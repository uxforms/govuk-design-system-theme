const assert = require('assert');
const mustache = require('mustache');
const fs = require('fs');
const cheerio = require('cheerio');

function render(template, args) {
    return cheerio.load(mustache.render(template, args));
}

describe('error-message', function () {
    const template = fs.readFileSync(__dirname + '/../src/templates/error-message/template.mustache', 'utf-8');

    it('should write an id attribute if given', function() {
        const $withoutId = render(template);
        assert.ifError($withoutId('span').attr('id'));

        const $withId = render(template, {errorMessage: {id: "45"}});
        assert.strictEqual($withId('span').attr('id'), "45");
    });

    it('should escape html in errorMessage.text', function () {
        // don't try to parse the result as html as all of the parsers I tried *unescaped* the value :(
        const result = mustache.render(template, {errorMessage: {text: "Here is some <i>markup</i>"}})
        assert.match(result, /Here is some &lt;i&gt;markup&lt;&#x2F;i&gt;/);
    });

    it('should leave not escape html in errorMessage.html', function() {
        // don't try to parse the result as html as all of the parsers I tried *unescaped* the value :(
        const result = mustache.render(template, {errorMessage: {html: "Here is some <i>markup</i>"}});
        assert.match(result, /Here is some <i>markup<\/i>/);
    });

    it('should write a default value when there is no visuallyHiddenText param given', function() {
        const $withDefault = render(template);
        assert.strictEqual($withDefault('span.govuk-visually-hidden').parent().html().trim(), '<span class="govuk-visually-hidden">Error:</span>');

        const $overridden = render(template, {errorMessage: {visuallyHiddenText: "My override"}});
        assert.strictEqual($overridden('span.govuk-visually-hidden').parent().html().trim(), '<span class="govuk-visually-hidden">My override:</span>');
    });

    it('should add classes to the outermost span, if specified', function() {
        const $withoutAdditionalClasses = render(template);
        assert.strictEqual($withoutAdditionalClasses('span').attr('class').trim(), "govuk-error-message");

        const $withAdditionalClasses = render(template, {errorMessage: {classes: "first second"}});
        assert.strictEqual($withAdditionalClasses('span').attr('class').trim(), "govuk-error-message first second");
    })

    function countAttributes(elem) {
        return Object.keys(elem.get(0).attribs).length;
    }

    it('should add attributes to the outermost span, if specified', function() {
        const $withoutAttrs = render(template);
        assert.strictEqual(countAttributes($withoutAttrs('span')), 1);

        const $withAttrs = render(template, {errorMessage: {attributes: [{attribute: "first", value: "one"}, {attribute: "second", value: "two"}]}});
        assert.strictEqual(countAttributes($withAttrs('span')), 3);
        assert.strictEqual($withAttrs('span').attr('first'), "one");
        assert.strictEqual($withAttrs('span').attr('second'), "two");
    })

});