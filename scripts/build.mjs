import * as esbuild from "esbuild";
import {resolveToEsbuildTarget} from "esbuild-plugin-browserslist";
import browserslist from "browserslist";
import copyStaticFiles from "esbuild-copy-static-files";
import {explodedDir, govukFrontendDir} from "./constants.mjs";

const devOptions = {
  sourcemap: "linked"
}
const prodOptions = {
  sourcemap: "linked",
  minify: true
}

const js = (options) => esbuild.build({
  ...options, ...{
    entryPoints: ['src/js/govukDesignSystemTheme.js'],
    bundle: true,
    outdir: `${explodedDir}/js`,
    target: resolveToEsbuildTarget(browserslist('>0.25%, last 3 versions, Firefox ESR, not dead'), {
      printUnknownTargets: false
    })
  }
})


const staticAssets = esbuild.build({
  plugins: [
    copyStaticFiles({
      src: 'src/assets',
      dest: `${explodedDir}/assets`
    })
  ]
})

const govukAssets = esbuild.build({
  plugins: [
    copyStaticFiles({
      src: `${govukFrontendDir}/assets`,
      dest: `${explodedDir}/assets`
    })
  ]
})

export function dev() {
  Promise.allSettled([
    govukAssets,
    staticAssets,
    js(devOptions),
  ])
}

export function prod() {
  Promise.allSettled([
    govukAssets,
    staticAssets,
    js(prodOptions),
  ])
}