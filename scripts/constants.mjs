
export const targetDir = 'target';
export const explodedDir = `${targetDir}/exploded`;

export const govukFrontendDir = 'node_modules/govuk-frontend/govuk';
