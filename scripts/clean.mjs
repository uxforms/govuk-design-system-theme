import {deleteSync} from "del";
import {targetDir} from "./constants.mjs";

deleteSync(targetDir, {dryRun: false})
