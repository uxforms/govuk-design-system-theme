import {zip} from "bestzip";
import {targetDir, govukFrontendDir} from "./constants.mjs";

const zipFilename = `${process.env.npm_package_name}-fixtures-${process.env.npm_package_version}.zip`;
zip({
  cwd: `${govukFrontendDir}/components/`,
  source: '**/fixtures.json',
  destination: `${process.cwd()}/target/${zipFilename}`
}).then(() => {
  console.log(`${zipFilename} created`);
}).catch(err => {
  console.error(err.stack);
  process.exit(1);
});
