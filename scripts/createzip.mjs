import {zip} from "bestzip";
import {explodedDir} from "./constants.mjs";

const zipFilename = `${process.env.npm_package_name}-${process.env.npm_package_version}.zip`;
zip({
  cwd: explodedDir,
  source: '.',
  destination: `../${zipFilename}`
}).then(() => {
  console.log(`${zipFilename} created`);
}).catch(err => {
  console.error(err.stack);
  process.exit(1);
});
